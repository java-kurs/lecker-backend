-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema lecker_db
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `lecker_db` ;

-- -----------------------------------------------------
-- Schema lecker_db
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `lecker_db` DEFAULT CHARACTER SET utf8 ;
USE `lecker_db` ;

-- -----------------------------------------------------
-- Table `lecker_db`.`lieferant`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `lecker_db`.`lieferant` ;

CREATE TABLE IF NOT EXISTS `lecker_db`.`lieferant` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(150) NOT NULL,
  `strasse` VARCHAR(250) NULL,
  `hnr` VARCHAR(10) NULL,
  `ort` VARCHAR(150) NULL,
  `land` VARCHAR(3) NULL,
  `bundesland` VARCHAR(100) NULL,
  `www` VARCHAR(250) NULL,
  `www_menue` VARCHAR(250) NULL,
  `email` VARCHAR(199) NULL,
  `telefon` VARCHAR(90) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `lecker_db`.`kunde`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `lecker_db`.`kunde` ;

CREATE TABLE IF NOT EXISTS `lecker_db`.`kunde` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `vorname` VARCHAR(45) NULL,
  `nachname` VARCHAR(45) NULL,
  `adresse` VARCHAR(255) NULL,
  `plz` VARCHAR(10) NULL,
  `ort` VARCHAR(150) NULL,
  `land` VARCHAR(3) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `lecker_db`.`rating`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `lecker_db`.`rating` ;

CREATE TABLE IF NOT EXISTS `lecker_db`.`rating` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `id_lieferant` INT NOT NULL,
  `id_kunde` INT NOT NULL,
  `rating` INT NOT NULL,
  INDEX `fk_Lieferant_has_Kunde_Kunde1_idx` (`id_kunde` ASC),
  INDEX `fk_Lieferant_has_Kunde_Lieferant_idx` (`id_lieferant` ASC),
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_Lieferant_has_Kunde_Lieferant`
    FOREIGN KEY (`id_lieferant`)
    REFERENCES `lecker_db`.`lieferant` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Lieferant_has_Kunde_Kunde1`
    FOREIGN KEY (`id_kunde`)
    REFERENCES `lecker_db`.`kunde` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

USE `lecker_db` ;

-- -----------------------------------------------------
-- Placeholder table for view `lecker_db`.`avg_rating`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `lecker_db`.`avg_rating` (`id` INT, `avg_rate` INT, `count_ratings` INT, `name` INT, `bl` INT);

-- -----------------------------------------------------
-- View `lecker_db`.`avg_rating`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `lecker_db`.`avg_rating`;
DROP VIEW IF EXISTS `lecker_db`.`avg_rating` ;
USE `lecker_db`;
CREATE  OR REPLACE VIEW `avg_rating` AS
SELECT
    id_lieferant AS id,
    AVG(rating) AS avg_rate,
    COUNT(id_lieferant) AS count_ratings,
    lieferant.name AS name,
    lieferant.bundesland as bl
FROM
    lecker_db.rating
        JOIN
    lieferant ON rating.id_lieferant = lieferant.id
GROUP BY id_lieferant;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `lecker_db`.`lieferant`
-- -----------------------------------------------------
START TRANSACTION;
USE `lecker_db`;
INSERT INTO `lecker_db`.`lieferant` (`id`, `name`, `strasse`, `hnr`, `ort`, `land`, `bundesland`, `www`, `www_menue`, `email`, `telefon`) VALUES (1, 'Pizza Luigi', 'Am Wall', '7', 'Gelsenkirchen', 'DE', 'NW', 'www.luigi.de', 'www.luigi.de/menue', 'info@luigi.de', '0177 3232233');
INSERT INTO `lecker_db`.`lieferant` (`id`, `name`, `strasse`, `hnr`, `ort`, `land`, `bundesland`, `www`, `www_menue`, `email`, `telefon`) VALUES (2, 'Hallo Pizza', 'Hauptstr.', '12', 'Essen', 'DE', 'NW', 'www.hallo-pizza.de', 'www.hallo-pizza.de/karte', 'info@hallo-pizza.de', '0151 93342943');
INSERT INTO `lecker_db`.`lieferant` (`id`, `name`, `strasse`, `hnr`, `ort`, `land`, `bundesland`, `www`, `www_menue`, `email`, `telefon`) VALUES (3, 'Pizza Rainer', 'Musterweg', '9', 'Leipzig', 'DE', 'SN', 'www.pizza-rainer.de', 'www.pizza-rainer.de/shop', 'pizza@pizza-rainer.de', '0172 866 99 554');

COMMIT;


-- -----------------------------------------------------
-- Data for table `lecker_db`.`kunde`
-- -----------------------------------------------------
START TRANSACTION;
USE `lecker_db`;
INSERT INTO `lecker_db`.`kunde` (`id`, `vorname`, `nachname`, `adresse`, `plz`, `ort`, `land`) VALUES (1, 'Niclas', 'Kuschel', 'Hauptstr. 85', '53888', 'Gelsenkirchen', 'DE');
INSERT INTO `lecker_db`.`kunde` (`id`, `vorname`, `nachname`, `adresse`, `plz`, `ort`, `land`) VALUES (2, 'Marco', 'Täschner', 'Fockerstr. , 04275 Leipzig', '04275', 'Leipzig', 'DE');

COMMIT;


-- -----------------------------------------------------
-- Data for table `lecker_db`.`rating`
-- -----------------------------------------------------
START TRANSACTION;
USE `lecker_db`;
INSERT INTO `lecker_db`.`rating` (`id`, `id_lieferant`, `id_kunde`, `rating`) VALUES (1, 1, 2, 4);
INSERT INTO `lecker_db`.`rating` (`id`, `id_lieferant`, `id_kunde`, `rating`) VALUES (2, 2, 2, 5);
INSERT INTO `lecker_db`.`rating` (`id`, `id_lieferant`, `id_kunde`, `rating`) VALUES (3, 2, 1, 3);
INSERT INTO `lecker_db`.`rating` (`id`, `id_lieferant`, `id_kunde`, `rating`) VALUES (4, 3, 1, 5);

COMMIT;

