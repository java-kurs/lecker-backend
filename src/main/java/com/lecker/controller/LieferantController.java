package com.lecker.controller;

import com.lecker.entity.AvgRating;
import com.lecker.entity.Lieferant;
import com.lecker.repository.AvgRatingRepository;
import com.lecker.repository.LieferantRepository;
import java.util.Optional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

/**
 *
 * @author TaeschMa
 */
@RestController
@RequestMapping(path = "/api/lieferant")
@Slf4j
public class LieferantController {
    
    private LieferantRepository repo;
    private AvgRatingRepository aggregationRepo;

    public LieferantController(LieferantRepository repository, AvgRatingRepository aggregationRepo ) {
        this.repo = repository;
        this.aggregationRepo = aggregationRepo;
    }

    @CrossOrigin
    @GetMapping("/all")
    public Iterable<Lieferant> getLieferanten() {
        log.info("Anzahl der Lieferanten: " + repo.count());
        
        int countRatings = repo.countRatings();
        log.info("countRatings: " + countRatings);
        
        float countRatingsForLieferant = repo.avgRatingForLieferantId(2);
        log.info("countRatingsForLieferant: " + countRatingsForLieferant);
        
        
        
        Iterable<AvgRating> findAllAggregtions = aggregationRepo.findAll();
        log.info(findAllAggregtions.toString());
        
        Iterable<Lieferant> alleLieferanten = repo.findAll();
        return alleLieferanten;
    }
    
    /**
     * Abruf Aggregation für einen Lieferanten 
     * 
     * http://localhost:8096/api/lieferant/aggregation/2
     * 
     * @param id
     * @return 
     */
    @GetMapping("/aggregation/{id}")
    public Optional<AvgRating> getAggegation(@PathVariable("id") Integer id) {        
        return aggregationRepo.findById(id);
    }

    @PostMapping
    public ResponseEntity<?> saveLieferant(@RequestBody Lieferant lieferant) throws Exception {
        try {
            lieferant = repo.save(lieferant);
        }
        catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "BAD_REQUEST");
        }
        return ResponseEntity.ok(lieferant);
    }
        
}
