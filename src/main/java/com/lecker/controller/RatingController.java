package com.lecker.controller;

import com.lecker.entity.Rating;
import com.lecker.repository.RatingRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping(path = "/api/rating")
@Slf4j
public class RatingController {
    
    private RatingRepository repo;

    public RatingController(RatingRepository ratingRepository) {
        this.repo = ratingRepository;
    }
    
    
    @PostMapping
    public ResponseEntity<?> saveRating(@RequestBody Rating rating) throws Exception {
        try {
            rating = repo.save(rating);
        }
        catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "BAD_REQUEST");
        }
        return ResponseEntity.ok(rating);
    }
}
