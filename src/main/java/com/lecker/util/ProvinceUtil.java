package com.lecker.util;

import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class ProvinceUtil {

    private Map<String, String> provinces = new HashMap<>();

    public ProvinceUtil() {
        provinces.put("BW", "Baden-Württemberg");
        provinces.put("BY", "Bayern");
        provinces.put("BE", "Berlin");
        provinces.put("BB", "Brandenburg");
        provinces.put("HB", "Bremen");
        provinces.put("HA", "Hamburg");
        provinces.put("HE", "Hessen");
        provinces.put("MV", "Mecklenburg-Vorpommern");
        provinces.put("NI", "Niedersachsen");
        provinces.put("NW", "Nordrhein-Westfalen");
        provinces.put("RP", "Rheinland-Pfalz");
        provinces.put("SL", "Saarland");
        provinces.put("SN", "Sachsen");
        provinces.put("ST", "Sachsen-Anhalt");
        provinces.put("SH", "Schleswig-Holstein");
        provinces.put("TH", "Thüringen");
    }

    public boolean containsProvinceKey(String key) {
        return this.provinces.containsKey(key);
    }

    public String getProvince(String key) {
        String value = null;
        if(this.provinces.containsKey(key)) {
            value = this.provinces.get(key); 
        }
        return value;
    }

    public Map<String, String> getProvinces() {
        return this.provinces;
    }
}


