package com.lecker.web;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 *
 * @author TaeschMa
 */
@Controller
@Slf4j
public class IndexController {

    @GetMapping("/")
    public String startseite() {
        log.info("Startseite");
        return "startseite";
    }
}
