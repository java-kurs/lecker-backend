package com.lecker.entity;

import javax.persistence.*;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity(name = "rating")
@Getter
@Setter
@NoArgsConstructor
public class Rating {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "id_lieferant")
    private Integer idLieferant;

    @Column(name = "id_kunde")
    private Integer idKunde;
    
    private Integer rating;

}
