package com.lecker.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 *
 * @author TaeschMa
 */
@Entity(name = "avg_rating")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class AvgRating {
    @Id
    private Integer id;
    @Column(name = "avg_rate")
    private Double avgRate;
    @Column(name = "count_ratings")
    private Integer countRatings;
    @Column(name = "name")
    private String name;
    @Column(name = "bl")
    private String bundesland;
}
