package com.lecker.entity;

import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

import javax.persistence.OneToMany;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity(name = "lieferant")
@Getter
@Setter
@NoArgsConstructor
public class Lieferant {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    
    private String name;
    private String strasse;
    private String hnr;
    private String ort;
    private String land;
    private String bundesland;
    private String www;
    private String www_menue;
    private String email;
    private String telefon;
    private String profilbild;

    @OneToMany(mappedBy = "idLieferant")
    private Set<Rating> ratings;
    
    @ManyToMany
    @JoinTable(
            name = "lieferant_has_kategorie",
            joinColumns = @JoinColumn(name = "lieferant_id"),
            inverseJoinColumns = @JoinColumn(name = "kategorie_id"))
    private Set<Kategorie> kategorien;
    
}
