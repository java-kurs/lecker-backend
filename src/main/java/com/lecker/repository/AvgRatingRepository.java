package com.lecker.repository;

import com.lecker.entity.AvgRating;
import org.springframework.data.repository.CrudRepository;

public interface AvgRatingRepository extends CrudRepository<AvgRating, Integer> {
    
}
