package com.lecker.repository;

import com.lecker.entity.Lieferant;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface LieferantRepository extends CrudRepository<Lieferant, Integer> {

    @Query("Select count(*) from rating")
    int countRatings();
    
    @Query("Select avg(rating) from rating where id_kunde = ?1")
    float avgRatingForLieferantId(Integer id);

}
